import pickle

def getDataFromPickle(name):
    with open(name, 'rb') as datasetFromName:
        persons = pickle.load(datasetFromName)   
    return persons

from saveDataToPickle import CommitBehaviour #Persons are Class Objects CommitBahviour

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import warnings
warnings.filterwarnings("ignore") #ignore warning to have a clean output


datalist = ['accumulo', 'ant-ivy', 'archiva', 'aurora', 'calcite', 'cayenne', 
            'commons-bcel', 'commons-beanutils','commons-codec', 'commons-collections', 
            'commons-compress', 'commons-configuration', 'commons-dbcp', 'commons-digester', 
            'commons-imaging', 'commons-io', 'commons-jcs', 'commons-jexl', 'commons-lang', 
            'commons-math','commons-net', 'commons-rdf', 'commons-scxml', 'commons-validator', 
            'commons-vfs', 'deltaspike', 'falcon', 'flume', 'giraph',
            'kafka', 'knox', 'kylin', 'lens', 'mahout', 'nifi', 'nutch', 'opennlp', 'parquet-mr', 
            'pdfbox', 'pig', 'storm', 'struts', 'systemml', 'tez', 'tika', 'zeppelin']

#datalist = ['accumulo', 'ant-ivy', 'archiva', 'aurora', 'calcite', 'cayenne', 
#            'commons-bcel', 'commons-beanutils','commons-codec', 'commons-collections', 
#            'commons-compress', 'commons-configuration', 'commons-dbcp']

#datalist = ['kafka']
       

# make persons-commit-behaviour-array
from saveDataToPickle import saveDataToPickle
P = dict()
allP = np.array([])

print ('Extract:') 
wrongNames = []

for name in datalist:
    try:
        A = getDataFromPickle(name)
        #if this doesn't work try it with a download first
        P[name] = A
        allP = np.append(allP,A)
    except:
        saveDataToPickle(name)
     
        A = getDataFromPickle(name)
            
        P[name] = A
        allP = np.append(allP,A)
            
            #if download doesnt work, maybe the name is wrong
    
    print (name + str(', ' + str(np.shape(A)) + str(' persons')))  
#P = np.array(P)
#P is a 2d array of every persons commit behaviour, sorted by each dataset, 
#P['accumulo'] is the data from accumulo with length 61 (means 61 persons)
#P['kafka'] is the data from kafka with 49 persons and so on...
#allP is an 1d array of every perons Commit Behaviour



# classifier 
from Analyzer import Classifier as CLF
 
CLF(allP).analyzeWithClassifiers() #input: X = [0,1,2,3,4,1,10,0,0,0,102,0], ouput y = [0] //num cpmmits


# ARIMA - Number of Committers
from Analyzer import Analyze_DifferentCommitters as ADC

for data in datalist[:2]:
    ADC( P[ data ] ).ARIMA(1, 1, 0).plotResults()
    plt.show()


# ARIMA - Total Number of Commits
from Analyzer import Analyze_NumberCommits as ANC

for data in datalist[:2]:
    ANC( P[ data ] ).ARIMA(1, 1, 0).plotResults()
    plt.show()


# for i in range(5*4):
#     fig = plt.subplot(5, 4, i+1)
    
#     print('\n\nPredict how many different committers ({}):'.format(datalist[i]))
#     adc = ADC( P[datalist[i]] )
#     adc.ARIMA(1, 1, 0)
#     p = adc.plotResults()
#     #fig.xlabel('')
