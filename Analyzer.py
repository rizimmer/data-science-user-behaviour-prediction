import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# main analyzer
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier

class Classifier:
    def __init__(self, persons): #persons should be a 1d-list of all persons (class CommitBehaviour)
        self.P = persons
        
        
    def analyzeWithClassifiers(self):
        print('\nAnalyze with Classifiers:')
        def getMinLastQuarter(P): #every database has a different quarter-length, take the shortest one that there isn't any array overflow
            m = 8000
            for p in P:
                if len(p.qdf) < m:
                    m = len(p.qdf)
                    #print (m)
            return m
        
        def getTrainingData(P, arrayLength = 2):
            X = []
            y = []
            for p in P:
                X.append( p.qdf[-arrayLength:-1].values )
                #taking the last n=arrayLength quarters but not the 
                #last quarter(this will be y for prediction) ->
                y.append( p.qdf[-1:].values)
                
            X = np.asarray(X)[:,:,0] # shape = (numPersons, arrayLength)
            y = np.asarray(y)[:,:,0] # shape = (numPersons)
                
            return X,y
        
        def train(X,y):
        
            X_train, X_test, y_train, y_test = train_test_split(
                        X, y, test_size=0.33, random_state=42, shuffle=True)
                    
            names = ["Nearest Neighbors", "Linear SVM", "RBF SVM",
                     "Decision Tree", "Random Forest", "Neural Net", "AdaBoost"]
                    
            classifiers = [
                    KNeighborsClassifier(5),
                    SVC(kernel="linear", C=0.025),
                    SVC(gamma=2, C=1),
                    DecisionTreeClassifier(max_depth=5),
                    RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1),
                    MLPClassifier(alpha=1),
                    AdaBoostClassifier()]
        
            isActive = y_test>0
            isActive = isActive[:,0]
            
            scoreAlwaysZero = np.count_nonzero(np.zeros(len(isActive)) == isActive)/len(isActive)
            
            print ('Score:')
            print('Prediction always zero: ' + str(scoreAlwaysZero) + '%')
            
            best = 0
            bestClf = ''
            for name,clf in zip(names,classifiers):
                #print (name)
                clf.fit( X_train, y_train )
        
                predictIsActive = clf.predict(X_test)>0

                score = np.count_nonzero(predictIsActive == isActive)/len(isActive)
                if score > best:
                    bestClf = name
                    best = score
                print (name + str(': ') + str(score) + '%')
            
            print('Winner is Classifier ' + bestClf + ' with ' + str(best) + '%')
                
        X,y = getTrainingData(self.P, arrayLength = getMinLastQuarter(self.P)) #get training data with taking every person in list
        #train with special group: P = self.P[self.P.name=='kafka'] -> X,y = getTrainingData(arrayLength = getMinLastQuarter(P))
        train(X,y)



from sklearn.metrics import mean_squared_error
import statsmodels.tsa.arima_model

# How many different committers are expected in the next quarter?
class Analyze_DifferentCommitters:
    def __init__(self, persons): # i.e. df = dcdf (different committers data frame)
        #self.df = df
        self.name = persons[0].name
        
        print('\n\nPredict how many different committers ({}):'.format(self.name))
        
        #pd.DataFrame(P[0][0].qdf>0, dtype=int)
        self.numActivePersons = pd.DataFrame(persons[0].qdf>0, dtype=int)
        
        for person in persons[1:]:
            self.numActivePersons = self.numActivePersons + pd.DataFrame(person.qdf>0, dtype=int)
            
        #print (pd.DataFrame(persons[0].qdf>0, dtype=int)+pd.DataFrame(persons[1].qdf>0, dtype=int))
        #print (numActivePersons)
        
    #See https://machinelearningmastery.com/arima-for-time-series-forecasting-with-python/
    def ARIMA(self, p, d, q): #ARIMA parameters
        self.df = self.numActivePersons.values #make usable format for ARIMA
        
        #test and training data
        self.size = int(len(self.df) * 0.66)
        train, test = self.df[0:self.size], self.df[self.size:len(self.df)]
                    
        history = [x for x in train]
        self.predictions = list()
        
        for t in range(len(test)):
            #fit model on training set
            model = statsmodels.tsa.arima_model.ARIMA(history, order=(p,d,q))
            model_fit = model.fit(disp=0)  
            
            #prediction on test set
            output = model_fit.forecast()
            yhat = int(output[0])
            self.predictions.append(yhat)
            
            #true or expected values of our test set
            obs = test[t]
            history.append(obs)
            
            qu = self.size + t #quarter of prediction
            #print('quarter=%i, predicted=%i, expected=%i' % (qu, yhat, obs))
             
        #Error analysis
        error = mean_squared_error(test, self.predictions)
        #print('Test MSE: %.3f' % error)
        
        return self
    
    def plotResults(self):
        
        # plot
        plt.xlabel('quarter')
        plt.ylabel('Number of different Committers')
        
        plt.plot(self.df,'or', color='blue', ms=4)
        plt.plot(range(self.size,len(self.df)), self.predictions, 'or', color='red', ms=4)
        
        plt.plot(self.df, color='blue')
        p, = plt.plot(range(self.size,len(self.df)), self.predictions, color='red')
        
        plt.grid(True)
        plt.title('Dataset {}'.format(self.name))
        plt.legend(('True', 'Predicted'))
        
        return p
        #plt.show()
        

# How many commits are expected in the next quarter?
class Analyze_NumberCommits:
    def __init__(self, persons, group = 'unnamed'): # i.e. df = qdf (number commits per quarter data frame)
        self.name = group
        self.name = persons[0].name
        
        print('\n\nPredict how many commits in total, ({}):'.format(self.name))
        
        #pd.DataFrame(P[0][0].qdf>0, dtype=int)
        self.numAllCommits= pd.DataFrame(persons[0].qdf, dtype=int)
        
        for person in persons[1:]:
            self.numAllCommits = self.numAllCommits + pd.DataFrame(person.qdf, dtype=int)
    
        
    #We can use exactly the same model as above
    def ARIMA(self, p, d, q): #ARIMA parameters
        self.df = self.numAllCommits.values #make usable format for ARIMA
        
        #test and training data
        self.size = int(len(self.df) * 0.66)
        train, test = self.df[0:self.size], self.df[self.size:len(self.df)]
                    
        history = [x for x in train]
        self.predictions = list()
        
        for t in range(len(test)):
            #fit model on training set
            model = statsmodels.tsa.arima_model.ARIMA(history, order=(p,d,q))
            model_fit = model.fit(disp=0)  
            
            #prediction on test set
            output = model_fit.forecast()
            yhat = int(output[0])
            self.predictions.append(yhat)
            
            #true or expected values of our test set
            obs = test[t]
            history.append(obs)
            
            qu = self.size + t #quarter of prediction
            #print('quarter=%i, predicted=%i, expected=%i' % (qu, yhat, obs))
             
        #Error analysis
        error = mean_squared_error(test, self.predictions)
        #print('Test MSE: %.3f' % error)
   
        return self
    
    def plotResults(self):
        
        # plot
        plt.xlabel('quarter')
        plt.ylabel('Number of Committs')
        
        plt.plot(self.df, color='blue')
        plt.plot(range(self.size,len(self.df)), self.predictions, color='red')
        
        plt.plot(self.df,'or', color='blue', ms=4)
        p, = plt.plot(range(self.size,len(self.df)), self.predictions, 'or', color='red', ms=4)
        
        plt.grid(True)
        plt.title('Dataset {}'.format(self.name))
        plt.legend(('True', 'Predicted'))
        
        return p     


        
if __name__ == '__main__':
    pass