**Data Science and Big Data Analytics - Group Project**

For the project, we had access to a database actively used for research on open source software development. The database contains information collected from the version control system, the issue tracker, and the mailing lists of the projects. 

The database contains complete data for the following 49 Apache projects:
accumulo, ant-ivy, archiva, aurora, calcite, cayenne, commons-bcel, commons-beanutils, commons-codec, commons-collections, commons-compress, commons-configuration, commons-dbcp, commons-digester, commons-imaging, commons-io, commons-jcs, commons-jexl, commons-lang, commons-math, commons-net, commons-rdf, commons-scxml, commons-validator, commons-vfs, deltaspike, falcon, flume, giraph, httpscomponents-client, kafka, knox, kylin, lens, mahout, nifi, nutch, opennlp, parquet-mr, pdfbox, pig, storm, struts, systemml, tez, tika, wss4j, xerces2, zeppelin.


The task is to answer questions that provide information about the future of the projects. Based on the provided data following questions were of interest:

    Which developers will still be commiting source code in the next quarter?
    How many different committers are expected in the next quarter?
    How many commits are expected in the next quarter?


