#this program donwloads the data from database and saves it into a txt-file
import pandas as pd
import numpy as np
import pickle
import datetime
import time
import matplotlib.pyplot as plt

class CommitBehaviour:
    def __init__(self, df, firstCommit, lastCommit, databaseName='unnamed'):
        self.name = databaseName
        self.df = df
        
        def seconds(date):
            return time.mktime(date.timetuple())

        def year(date):
            return date.year
        
        def quarter(date):
            return date.quarter
        
        self.df['s'] = self.df['d'].apply(seconds)
        self.df['s'] = self.df['s'] - seconds(firstCommit) #set offset to zero (first commit at second '0')
        
        self.df['y'] = self.df['d'].apply(year)
        self.df['y'] = self.df['y'] - year(firstCommit) #set offset to zero
        
        self.df['q'] = self.df['d'].apply(quarter) + 4 * self.df['y']
        self.df['q'] = self.df['q'] - quarter(firstCommit) #set offset to zero
        
        self.lastQuarter = quarter(lastCommit) - quarter(firstCommit) + 4 * (year(lastCommit) - year(firstCommit)) #last active quarter of any person in database
                        
        #self.df is a DataFrame with entries [d', 's', 'y', 'q']
        #s = second of a commit after first commit of a person from a data base
        #d = abolut date in dtype = Timestamp 
        #y = relative year, starting from 0, first commit in year 0 (birth of jesus)
        #q = relative quarter starting from 0, first commit in quarter 0
            
        #DataFrame with the amount of commits each quarter ((q)uarter(d)ata(f)rame)
        self.qdf = pd.DataFrame({'n':self.df['q'].value_counts().reindex( range( self.lastQuarter + 1 ) ).fillna(0)})
        
        #DataFrame with amount of different committers each quarter (d)ifferent (c)ommitters
        #self.dcdf = pd.DataFrame({'nd': [self.df['id'][self.df['q']==i].nunique() for i in range(self.lastQuarter+1)]})

        self.df = self.df.drop(['id','d','y','q'], axis=1) #drop id date year and quarter, not needed anymore
        
    def isActive(self, quarter): #checks if a person if active a specific quarter
        return self.qdf[quarter:quarter+1] > 0
    
    def activeQuarters(self):
        return 
    
    def plot_isActive(self,labels=True):
        plt.xlim([0,self.lastQuarter])
        if labels:
            plt.xticks(range(self.lastQuarter+1))
            plt.yticks([])
            plt.xlabel('Quarter q')
            plt.title('blue=active')
        else:
            plt.xticks([])
            plt.yticks([])
        plt.imshow( np.asarray(self.qdf > 0).T, 'Blues', vmin=0, vmax=1)
        plt.show()
    
    def plot_df(self):
        plt.plot(self.df['s'], np.zeros(len(self.df['s'])), 'or', ms=0.4)
        plt.xlabel("Seconds after first commit")
        plt.show()
    
    def plot_qdf(self):
        plt.plot(self.qdf,'or')
        plt.xlabel("Quarter q")
        plt.ylabel("Num Commits n")
        plt.show()
    
    def plot_commitTimeDiff(self):
        plt.xlabel('Commit number')
        plt.ylabel('Time difference to next commit')
        plt.plot(np.diff(self.df['s']), 'or', ms=0.5)
        plt.show()
    
    
from mongoengine import connect
from pycoshark.mongomodels import Commit, Project, VCSSystem
from pycoshark.utils import create_mongodb_uri_string

def getCommitData(name):
    # Database credentials
    user = 'datascience2018'
    password = 'qFztn73TwV'
    host = '134.76.81.151'
    port = '27017'
    authentication_db = 'smartshark_test'
    database = "smartshark_test"
    ssl_enabled = None
    
    # Establish connection
    uri = create_mongodb_uri_string(user, password, host, port, authentication_db, ssl_enabled)
    connect(database, host=uri)

    # Fetch project id and version control system id for the 'kafka' project
    # The only() decides the data that is actually retrieved from the MongoDB. Always restrict this to the field that you require!
    project = Project.objects(name=name).only('id').get()
    vcs_system = VCSSystem.objects(project_id=project.id).only('id','url').get()
    #print('url of VCS system of the project: %s' % vcs_system.url)
    print('Download data from %s' % vcs_system.url)
    
    committerDate = []
    committerId = []
    #committerTime = []

    for commit in Commit.objects(vcs_system_id=vcs_system.id).only('committer_date', 'committer_id').timeout(False):
        committerDate.append(commit.committer_date)
        committerId.append(str(commit.committer_id))

    #DataFrame with sorted dates of every commit, with the information who made the commit (id)
    df = pd.DataFrame({"id":committerId, 
                       "d":committerDate}) 
    
    valueCounts = df['id'].value_counts()
    valueIndex = valueCounts.index

    a = []
    for i in range(len(valueCounts)):
        x = df[ df['id'] == valueIndex[i] ]
        a.append(x)
        
    return a

def saveDataToPickle(name):  
    persons = getCommitData(name)
    firstCommit = datetime.datetime.max
    lastCommit = datetime.datetime.min #will be the start/end-time for each database
    
    personsData = []
    for person in persons: #x is a single person->sort commit-dates by value and add it to 'data'
        person = person.sort_values(by='d').reset_index(drop=True) #sort by date and reset index
        if person['d'][0] < firstCommit:
            firstCommit = person['d'][0] #first entry is lowest date
        if person['d'].iloc[-1] > lastCommit:
            lastCommit = person['d'].iloc[-1] #last entry is highest date
                    
        personsData.append( person )
                  
    #sum up every Persons CommitBehaviour in A and save it into a file named 'name'  
    A = [ CommitBehaviour( person, firstCommit, lastCommit, databaseName = name ) for person in personsData ]
    
    with open(name, 'wb') as handle:
        pickle.dump(A, handle)


datalist = ['accumulo', 'ant-ivy', 'archiva', 'aurora', 
            'calcite', 'cayenne', 'commons-bcel', 'commons-beanutils',
            'commons-codec', 'commons-collections', 'commons-compress', 'commons-configuration', 
            'commons-dbcp', 'commons-digester', 'commons-imaging', 'commons-io', 
            'commons-jcs', 'commons-jexl', 'commons-lang', 'commons-math',
            'commons-net', 'commons-rdf', 'commons-scxml', 'commons-validator', 
            'commons-vfs', 'deltaspike', 'falcon', 'flume', 
            'giraph','kafka', 'knox', 'kylin', 
            'lens', 'mahout', 'nifi', 'nutch', 
            'opennlp', 'parquet-mr', 'pdfbox', 'pig', 
            'storm', 'struts', 'systemml', 'tez', 
            'tika', 'zeppelin']

datalist = ['kafka']   

if __name__ == '__main__':
    for name in datalist:
        saveDataToPickle(name)







